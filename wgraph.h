/**
 * Weighted, Undirected Graph
 * @author Charlie Crouse
 * @version 1.0.0
 * @since 2017-07-31
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is a part of a collection of C programs that construct a library  *
 *  meant for personal use.                                                    *
 *                                                                             *
 *  Copyright (C) 2017 Charlie T. Crouse                                       *
 *                                                                             *
 *  This program is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation, either version 3 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef WGRAPH_H
#define WGRAPH_H

typedef struct edge {
  int v;
  int w;
  double weight;
  struct edge *next;
} edge;

typedef struct wgraph {
  int V;
  int E;
  edge **adj_list;
} wgraph;

/**
 * allocates a buffer for an edge
 * @return the edge buffer
 */
edge *edge_alloc();

/**
 * initializes and allocates memory for an edge
 * @param e      A pointer to the edge to initialize
 * @param v      The first vertex
 * @param w      The second vertex
 * @param weight The weight of the edge
 */
void edge_init(edge **e, int v, int w, double weight);

/**
 * allocates a buffer for a wgraph
 * @return the wgraph buffer
 */
wgraph *wgraph_alloc();

/**
 * initializes and allocates memory for a wgraph
 * @param wg A pointer to the wgraph to initialize
 * @param V  The number of vertexes the graph should contain
 */
void wgraph_init(wgraph **wg, int V);

/**
 * initializes a graph from data contained in a given file
 * @note the file should contain the number of vertexes, edges, and then the
 * edges as two integers then a double (weight) followed by a newline
 * @param wg       A pointer to the wgraph to initialize
 * @param filename The name of the file that contains the data
 */
void wgraph_init_from_file(wgraph **wg, char *filename);

/**
 * frees all allocated resources for a wgraph
 * @param wg A pointer to the wgraph to free
 */
void wgraph_free(wgraph *wg);

/**
 * adds a connection from 'v' to 'w'
 * @param wg     A pointer to a wgraph to add the connection to
 * @param v      The first vertex
 * @param w      The second vertex
 * @param weight The weight of the edge
 */
void wgraph_add_edge(wgraph *wg, int v, int w, double weight);

/**
 * determines if two vertexes are connected
 * @param  wg A pointer to a wgraph that will be checked for the connection
 * @param  v  The first vertex
 * @param  w  The second vertex
 * @return    (1) if the two vertexes are adjacent and (0) otherwise
 */
int wgraph_connected(wgraph *wg, int v, int w);

/**
 * prints a formatted wgraph to stdout
 * @param wg A pointer to a wgraph to print
 */
void wgraph_print(wgraph *wg);


// ---------------------------- Implementations ----------------------------- //

static void validate_vertex(wgraph *wg, int v);

edge *edge_alloc()
{
  edge *e = (edge *)malloc(sizeof(edge));

  if(e == NULL) {
    fprintf(stderr, "  Error: 'wgraph.h, edge_alloc' | invalid call to malloc()\n");
    exit(EXIT_FAILURE);
  }

  return e;
}

void edge_init(edge **e, int v, int w, double weight)
{
  if(*e == NULL) {
    *e = edge_alloc();
  }

  (*e)->v      = v;
  (*e)->w      = w;
  (*e)->weight = weight;
  (*e)->next   = NULL;
}

wgraph *wgraph_alloc()
{
  wgraph *wg = (wgraph *)malloc(sizeof(wgraph));

  if(wg == NULL) {
    fprintf(stderr, "  Error: 'wgraph.h, wgraph_alloc' | invalid call to malloc()\n");
    exit(EXIT_FAILURE);
  }

  return wg;
}

void wgraph_init(wgraph **wg, int V)
{
  if(*wg == NULL) {
    *wg = wgraph_alloc();
  }

  (*wg)->V = V;
  (*wg)->E = 0;

  (*wg)->adj_list = (edge **)malloc(sizeof(edge) * V);

  if((*wg)->adj_list == NULL) {
    fprintf(stderr, "  Error: 'wgraph.h, wgraph_init' | invalid call to malloc()\n");
    exit(EXIT_FAILURE);
  }

  for(int i = 0; i < V; i++) {
    (*wg)->adj_list[i] = NULL;
  }
}

void wgraph_init_from_file(wgraph **wg, char *filename)
{
  FILE *fp = fopen(filename, "r");

  if(fp == NULL) {
    fprintf(stderr, "  Error: 'wgraph.h, wgraph_init_from_file' | invalid file %s\n", filename);
    exit(EXIT_FAILURE);
  }

  int V, N;
  fscanf(fp, "%d\n%d", &V, &N);


  if(*wg != NULL) {
    wgraph_free(*wg), *wg = NULL;
  }

  wgraph_init(wg, V);

  int v, w;
  double weight;

  while(fscanf(fp, "%d %d %lf", &v, &w, &weight) > 0) {
    validate_vertex(*wg, v);
    validate_vertex(*wg, w);
    wgraph_add_edge(*wg, v, w, weight);
  }
}

void wgraph_free(wgraph *wg)
{
  // TODO:
}

void wgraph_add_edge(wgraph *wg, int v, int w, double weight)
{
  if(wg == NULL) {
    fprintf(stderr, "  Error: 'wgraph.h, wgraph_add_edge' | wgraph is NULL\n");
    exit(EXIT_FAILURE);
  }

  validate_vertex(wg, v);
  validate_vertex(wg, w);

  edge *p = NULL;
  edge_init(&p, v, w, weight);
  p->next = wg->adj_list[v];
  wg->adj_list[v] = p;

  edge *s = NULL;
  edge_init(&s, w, v, weight);
  s->next = wg->adj_list[w];
  wg->adj_list[w] = s;
}

int wgraph_connected(wgraph *wg, int v, int w)
{
  return 0;
  // TODO:
}

void wgraph_print(wgraph *wg)
{
  if(wg == NULL) {
    fprintf(stderr, "  Error: 'wgraph.h, wgraph_print' | wgraph is NULL\n");
    exit(EXIT_FAILURE);
  }

  for(int i = 0; i < wg->V; i++) {
    printf("%d: ", i);

    for(edge *p = wg->adj_list[i]; p != NULL; p = p->next) {
      printf("(%d, %lf)%s", p->w, p->weight, (p->next == NULL) ? "" : ", ");
    }

    printf("\n");
  }
}

static void validate_vertex(wgraph *wg, int v)
{
  if(v >= wg->V) {
    fprintf(stderr, "  Error: 'wgraph.h' vertex %d is not less than %d\n", v, wg->V);
    exit(EXIT_FAILURE);
  }
}




















#endif // WGRAPH_H
