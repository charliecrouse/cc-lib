/**
 * Hashtable
 *
 * Implementation of a generic hashtable.
 * example on how to use this file is provided at the following url
 * https://github.com/crouse0/CCLib/blob/master/examples/hashtable/hashtable.example.c
 * @author Charlie Crouse
 * @version 1.0.1
 * @since 2017-07-30
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is a part of a collection of C programs that construct a library  *
 *  meant for personal use.                                                    *
 *                                                                             *
 *  Copyright (C) 2017 Charlie T. Crouse                                       *
 *                                                                             *
 *  This program is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation, either version 3 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <stdio.h>
#include <stdlib.h>

/**
 * typedef for a hashing function pointer
 * @param  key The item to hash
 * @return     The resulting hash value
 */
typedef size_t (*hash_func)(const void *key);

/**
 * typedef for a comparison function pointer
 * @param  a The first item
 * @param  b The second item
 * @return   The resulting comparison value (<0 if a < b, > 0 if a > b, and 0 if a == b)
 */
typedef int (*cmp_func)(const void *a, const void *b);

/**
 * typedef for a function that will free the keys
 * @note    if the keys have no memory allocated, pass a function that does nothing
 *          See the end of this file for sample deallocation funcitons
 * @param f The item to deallocate
 */
typedef void (*free_key)(void *f);

/**
 * typedef for a function that will free the values
 * @note    if the values have no memory allocated, pass a function that does
 * nothing
 * @param f The item to deallocate
 */
typedef void (*free_val)(void *f);

/**
 * typedef for a print function that prints a generic value
 * @param p The item to print
 */
typedef void(*print)(void *p);

typedef struct node {
  const void *key;
  void *value;
  struct node *next;
} node;

typedef struct hashtable {
  size_t len;
  node **buckets;
  hash_func hash;
  cmp_func cmp;
  free_key fkey;
  free_val fval;
} hashtable;

/**
 * allocates a buffer for a node
 * @return the node buffer
 */
static node *node_alloc();

/**
 * allocates and initializes memory for a new node
 * @param p     A pointer to a node to initialize
 * @param key   The key to insert into the node
 * @param value The value to associate with the key
 */
void node_init(node **p, void *key, void *value);

/**
 * allocates a buffer for a hashtable
 * @return The hashtable buffer
 */
static hashtable *hashtable_alloc();

/**
 * allocates and initializes memory for a new hashtable
 * @param ht   A pointer to the hashtable to initialize
 * @param len  The initial size of the table (primes offer better performance)
 * @param hash A pointer to a hashing function
 * @param cmp  A pointer to a comparison function
 * @param fkey A pointer to a function used to free the keys
 * @param fval A pointer to a function used to free the values
 */
void hashtable_init(hashtable **ht, size_t len, hash_func hash, cmp_func cmp, free_key fkey, free_val fval);

/**
 * frees all allocated resources for a hashtable
 * @param ht The hashtable to free
 */
void hashtable_free(hashtable *ht);

/**
 * performs a lookup operation in a hashtable
 * @param  ht  A pointer to a hashtable to search in
 * @param  key The key of the node to search for
 * @return     The value of the key found or (NULL) if the key was not found
 */
void *hashtable_find(hashtable *ht, void *key);

/**
 * inserts a new key-value node into a hashtable
 * @param  ht    A pointer to the hashtable to insert the node into
 * @param  key   The key of the node to be inserted
 * @param  value The value to associate to the key
 * @return       (1) if the item was inserted successfully (2) if the item
 *                already exists and was updated, and (0) otherwise
 */
void hashtable_insert(hashtable *ht, void *key, void *value);

/**
 * removes a key-value node from a hashtable
 * @param  ht  A pointer to a hashtable to remove the node from
 * @param  key The key of the node to be removed
 * @return     (1) if the item was removed successfully and (0) if the item does
 *              not exist in the hashtable
 */
int hashtable_remove(hashtable *ht, void *key);

/**
 * prints a formatted hashtable
 * @param keyp A pointer to a print function that will print the keys
 * @param valp A pointer to a print function that will print the values
 */
void hashtable_print(hashtable *ht, print keyp, print valp);


// ---------------------------- Implementations ----------------------------- //

static node *node_alloc()
{
  node *p = (node *)malloc(sizeof(node));

  if(!p) {
    fprintf(stderr, "  Error: 'hashtable.h, node_alloc' | invalid call to malloc()");
    exit(EXIT_FAILURE);
  }

  return p;
}

void node_init(node **p, void *key, void *value)
{
  if(!*p) {
    *p = node_alloc();
  }

  (*p)->key   = key;
  (*p)->value = value;
  (*p)->next  = NULL;
}

static hashtable *hashtable_alloc()
{
  hashtable *ht = (hashtable *)malloc(sizeof(hashtable));

  if(!ht) {
    fprintf(stderr, "  Error: 'hashtable.h, hashtable_alloc' | invalid call to malloc()\n");
    exit(EXIT_FAILURE);
  }

  return ht;
}

void hashtable_init(hashtable **ht, size_t len, hash_func hash, cmp_func cmp, free_key fkey, free_val fval)
{
  *ht = hashtable_alloc();  // will allocate over previous table if not NULL

  if(len < 0) {
    fprintf(stderr, "  Error: 'hashtable.h, hashtable_init' | the size of the table must be positive\n");
    exit(EXIT_FAILURE);
  }

  (*ht)->len     = len;
  (*ht)->buckets = (node **)malloc(sizeof(node *) * len);

  if(!(*ht)->buckets) {
    fprintf(stderr, "  Error: 'hashtable.h, hashtable_init' | invalid call to malloc()\n");
    exit(EXIT_FAILURE);
  }

  for(int i = 0; i < len; i++) {
    (*ht)->buckets[i] = NULL;
  }

  (*ht)->hash = hash;
  (*ht)->cmp  = cmp;
  (*ht)->fkey = fkey;
  (*ht)->fval = fval;
}

void hashtable_free(hashtable *ht)
{
  if(!ht) {
    fprintf(stderr, "  Error: 'hashtable.h, hashtable_free' | invalid call to malloc()\n");
    exit(EXIT_FAILURE);
  }

  for(int i = 0; i < ht->len; i++) {
    node *p = ht->buckets[i], *s = NULL;
    while((s = p) != NULL) {
      p = p->next;

      ht->fkey((void *)s->key);
      ht->fval(s->value);

      free(s), s = NULL;
    }
  }

  free(ht->buckets), ht->buckets = NULL;
  free(ht), ht = NULL;
}

void *hashtable_find(hashtable *ht, void *key)
{
  if(!ht) {
    fprintf(stderr, "  Error: 'hashtable.h, hashtable_find' | hashtable is NULL\n");
    exit(EXIT_FAILURE);
  }

  size_t hashval = ht->hash(key) % ht->len;

  for(node *p = ht->buckets[hashval]; p != NULL; p = p->next) {
    if(!ht->cmp(key, p->key)) {
      return p->value;
    }
  }

  return NULL;
}

void hashtable_insert(hashtable *ht, void *key, void *value)
{
  if(!ht) {
    fprintf(stderr, "  Error: 'hashtable.h, hashtable_insert' | hashtable is NULL\n");
    exit(EXIT_FAILURE);
  }

  if(!key || !value) {
    fprintf(stderr, "  Error: 'hashtable.h hashtable_insert' | %s is invalid\n", !key ? "key" : "value");
    exit(EXIT_FAILURE);
  }

  size_t hashval = ht->hash(key) % ht->len;

  node *p = NULL;
  node_init(&p, key, value);

  p->next              = ht->buckets[hashval];
  ht->buckets[hashval] = p;
}

int hashtable_remove(hashtable *ht, void *key)
{
  if(!ht) {
    fprintf(stderr, "  Error: 'hashtable.h, hashtable_remove' | hashtable is NULL\n");
    exit(EXIT_FAILURE);
  }

  if(!key) {
    fprintf(stderr, "  Error: 'hashtable.h, hashtable_remove' | key is invalid\n");
    exit(EXIT_FAILURE);
  }

  size_t hashval = ht->hash(key) % ht->len;

  node *p, *s;

  for(p = s = ht->buckets[hashval]; p != NULL; s = p, p = p->next) {
    if(!ht->cmp(key, p->key)) {
      if(p == ht->buckets[hashval]) {
        ht->buckets[hashval] = p->next;
      } else {
        s->next = p->next;
      }

      free(p), p = NULL;
      return 1;
    }
  }

  return 0;
}

void hashtable_print(hashtable *ht, print keyp, print valp)
{
  if(!ht) {
    fprintf(stderr, "  Error: 'hashtable.h, hashtable_print' | hashtable is NULL\n");
    exit(EXIT_FAILURE);
  }

  for(int i = 0; i < ht->len; i++) {
    if(ht->buckets[i] != NULL) {
      printf("%d: ", i);

      for(node *p = ht->buckets[i]; p != NULL; p = p->next) {
        keyp((void *)p->key);
        printf(" => ");
        valp(p->value);
        printf("%s", p->next == NULL ? "" : ", ");
      }

      printf("\n");
    }
  }
}

// ------------------------ Sample Hashing Functions ------------------------ //

size_t string_hash(const void *key)
{
  char *p = (char *)key;

  unsigned hashval;

  for(hashval = 0; *p != 0; p++) {
    hashval = *p + 31 * hashval;
  }

  return hashval;
}

size_t int_hash(const void *key)
{
  return (int)((long)key) & 0x7FFFFFFF;
}

// ----------------------- Sample Comparison Functions ---------------------- //

int str_cmp(const void *a, const void *b)
{
  while(*(char *)a && (*(char *)a == *(char *)b)) {
    a++, b++;
  }

  return *(const unsigned char *)a - *(const unsigned char *)b;
}

int int_cmp(const void *a, const void *b)
{
  return (int)((long)a) - (int)((long)b);
}

// ---------------------- Sample Deallocation Functions --------------------- //

void do_nothing(void *f)
{
  // does nothing
}

void free_int(void *f)
{
  do_nothing(f);
}

void free_string(void *f)
{
  if(f) free((char *)f);
}

// ------------------------ Sample Printing Functions ----------------------- //

void print_string(void *p)
{
  printf("%s", (char *)p);
}

void print_int(void *p)
{
  printf("%d", (int)(long)p);
}

#endif
