#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "../../hashtable.h"
#include "../../read.h"

/**
 * This is a simple example to show how to use hashtable.h to make a
 * string dictionary
 */

/** setup */

#define SIZE 2039

typedef hashtable dictionary;

// hash function defined in hashtable.h
size_t string_hash(const void *key);

// comparison function defined in hashtable.h
int _strcmp(const void *a, const void *b);

// function that will free the keys and values defined in hashtable.h
void free_string(void *f);

/** dictionary functions */

// initializes a new dictionary
dictionary *dict_init()
{
  dictionary *dict;
  hashtable_init(&dict, SIZE, string_hash, _strcmp, free_string, free_string);

  return dict;
}

// inserts a new key value pair into the dictionary
void dict_insert(dictionary *dict, char *key, char *val)
{
  hashtable_insert(dict, (void *)key, (void *)val);
}

// finds the given target in the dictionary
char *dict_find(dictionary *dict, char *target)
{
  return (char *)hashtable_find(dict, (void *)target);
}

/** that is all the setup required */

// driver to demonstrate functionality
int main(int argc, char const *argv[])
{
  dictionary *dict = dict_init();

  printf("enter the number of entries to insert: ");
  int N = read_int();

  for(int i = 0; i < N; i++) {
    printf("\n\n Enter the key: ");
    char *key = read_string();
    printf("\n Enter the value: ");
    char *value = read_line();

    dict_insert(dict, key, value);
  }

  while(1) {
    printf("\n Enter the key to search for (type quit to exit):\t");

    char *cmd = read_string();

    if(!strcasecmp(cmd, "quit") || !strcasecmp(cmd, "q")) break;

    char *target = cmd;

    char *got = dict_find(dict, target);

    printf("\t%s => %s\n\n", target, got ? got : "NULL");

  }

  hashtable_free(dict);

  return 0;
}
