#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "../../hashtable.h"
#include "../../read.h"

/**
 * This is a complex example to show how to use hashtable.h to make a hashtable
 * that maps strings to doubles as key value pairs
 *
 * This is a lot of setup, yes, but my intention with hashtable.h was to be
 * versatile and work with various mappints
 */

/** hashtable setup */

#define SIZE 2039

// rename the type to make clear that it holds strings maped to doubles
typedef hashtable str_to_dbl;

/** function definitions for the hashtable */

// values that are not easily casted between (void *) rely on a
// struct container to be used
typedef struct {
  double dbl;
} container;

// hash function to hash the string keys (defined in hashtable.h)
size_t string_hash(const void *key);

// comparison function to compare the string keys (defined in hashtable.h)
int _strcmp(const void *a, const void *b);

// function that frees the keys (defined in hashtable.h)
void free_string(void *f);

// function to free our values (the double containers)
void free_container(void *f)
{
  if(f) free((void *)(container *)f);
}

// allocates a new container buffer
container *container_alloc()
{
  container *ct = (container *)malloc(sizeof(container));
  if(!ct) {
    fprintf(stderr, "  Oops! Couldn't allocate memory for a new containter.\n");
    exit(EXIT_FAILURE);
  }

  return ct;
}

/** str_to_dbl functions */

// initializes a container with the given double
void container_init(container **ct, double dbl)
{
  (*ct) = container_alloc();
  (*ct)->dbl = dbl;
}

// initialize a new table that maps strings to doubles
hashtable *str_to_dbl_init()
{
  hashtable *ht;
  hashtable_init(&ht, SIZE, string_hash, _strcmp, free_string, free_container);

  return ht;
}

// inserts an element into the str_to_dbl table
void str_to_dbl_insert(str_to_dbl *mytable, char *key, double val)
{
  container *ct;
  container_init(&ct, val);

  hashtable_insert(mytable, (void *)key, (void *)ct);
}

// finds an element from the str_to_dbl table
double str_to_dbl_find(str_to_dbl *mytable, char *target)
{
  if(!mytable) {
    fprintf(stderr, "  Oops! The given table is invalid.\n");
    exit(EXIT_FAILURE);
  }

  if(!target) {
    fprintf(stderr, "  Oops! The given key is invalid.\n");
    exit(EXIT_FAILURE);
  }

  container *got;
  got = hashtable_find(mytable, (void *)target);

  if(!got) {
    printf("The key %s was not found in the table, garbage value has been returned.\n", target);
    return (double)(long) NULL;
  }

  return ((container *)got)->dbl;
}


int main(int argc, char const *argv[])
{
  str_to_dbl *mytable = str_to_dbl_init();

  int N = read_int();  // number of keys to insert

  for(int i = 0; i < N; i++) {
    char *key = read_string();
    double val = read_double();

    str_to_dbl_insert(mytable, key, val);
  }

  while(1) {
    printf("\n Enter the key to search for (type quit to exit):\t");

    char *cmd = read_string();

    if(!strcasecmp(cmd, "quit") || !strcasecmp(cmd, "q")) break;

    char *target = cmd;
    double got = str_to_dbl_find(mytable, target);

    printf("\n  %s => %lf\n\n", target, got);
  }

  hashtable_free(mytable);

  return 0;
}













