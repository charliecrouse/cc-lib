/**
 * Input Library
 *
 * @author Charlie Crouse
 * @version 1.0.0
 * @since 2017-07-15
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is a part of a collection of C programs that construct a library  *
 *  meant for personal use.                                                    *
 *                                                                             *
 *  Copyright (C) 2017 Charlie T. Crouse                                       *
 *                                                                             *
 *  This program is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation, either version 3 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef READ_H
#define READ_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

/**
 * determines if there are more bytes in the stdin stream
 * @return (1) if stdin is empty and (0) otherwise
 */
int is_empty();

/**
 * determines if there are more bytes in the fp stream
 * @return (1) if fp is empty and (0) otherwise
 */
int fis_empty(FILE *fp);

/**
 * determines if the stdin stream has more bytes to read
 * @return (1) if the stdin stream is not empty and (0) otherwise
 */
int has_next();

/**
 * determines if the fp stream has more bytes to read
 * @return (1) if the fp stream is not empty and (0) otherwise
 */
int fhas_next(FILE *fp);

/**
 * attempts to read a stream of chars until a newline or EOF character
 * is found.
 * @return a pointer to the characters read
 */
char *read_line();

/**
 * attempts to read a stream of chars until a newline or EOF character
 * is found.
 * @return a pointer to the characters read
 */
char *fread_line(FILE *fp);

/**
 * attempts to read the remainder of the stdin stream
 * @return a pointer to the chars read
 */
char *read_all();

/**
 * attempts to read the remainder of the fp stream
 * @return a pointer to the chars read
 */
char *fread_all(FILE *fp);

/**
 * attempts to read the next string of text from the stdin stream
 * @return a pointer to the chars read
 */
char *read_string();

/**
 * attempts to read the next string of text from the fp stream
 * @return a pointer to the chars read
 */
char *fread_string(FILE *fp);

/**
 * attempts to read an int from the stdin stream
 * @return the int read
 */
int read_int();

/**
 * attempts to read an int from the fp stream
 * @return the int read
 */
int fread_int(FILE *fp);

/**
 * attempts to read an float from the stdin stream
 * @return the int read
 */
float read_float();

/**
 * attempts to read an float from the fp stream
 * @return the int read
 */
float fread_float(FILE *fp);

/**
 * attempts to read an double from the stdin stream
 * @return the int read
 */
double read_double();

/**
 * attempts to read an double from the fp stream
 * @return the int read
 */
double fread_double(FILE *fp);

/**
 * attempts to read an long from the stdin stream
 * @return the int read
 */
long read_long();

/**
 * attempts to read an long from the fp stream
 * @return the int read
 */
long fread_long(FILE *fp);

/**
 * attempts to read a short from the stdin stream
 * @return the int read
 */
short read_short();

/**
 * attempts to read a short from the fp stream
 * @return the int read
 */
short fread_short(FILE *fp);

/**
 * attempts to read the remaining strings from the stdin stream
 * @returns A pointer to the array of strings read
 */
char **read_all_strings();

/**
 * attempts to read the remaining strings from the fp stream
 * @returns A pointer to the array of strings read
 */
char **fread_all_strings(FILE *fp);

/**
 * attempts to read the remaining lines from the stdin stream
 * @return A pointer to the array of lines read
 */
char **read_all_lines();

/**
 * attempts to read the remaining lines from the fp stream
 * @return A pointer to the array of lines read
 */
char **fread_all_lines(FILE *fp);


// ---------------------------- Implementations ----------------------------- //

static size_t __strlen(const char *s);

static char *__strcat(char *dest, const char *src);

static char *__strdup(char *s);

int fis_empty(FILE *fp)
{
  fpos_t pos;
  fgetpos(fp, &pos);

  int c;

  while((c = fgetc(fp)) && isspace(c) && c != EOF)
    ;

  fsetpos(fp, &pos);
  return c == EOF ? 1 : 0;
}

int fhas_next(FILE *fp)
{
  return (fis_empty(fp) == 1) ? 0 : 1;
}


char *fread_line(FILE *fp)
{
  // *!*
  int c;
  while((c = fgetc(fp)) != EOF && isspace(c))
    ;
  ungetc(c, fp);

  // *!*
  size_t len;
  char *line = NULL;

  if((int)(len = getline(&line, &len, fp)) <= 0) {
    return NULL;
  }

  if(line[len - 1] == '\n') {
    line[len - 1] = '\0';
  }

  return line;
}


char *fread_all(FILE *fp)
{
  char *all = NULL;
  char *line = NULL;
  size_t all_len = 0, line_len;

  while((int)(line_len = getline(&line, &line_len, fp)) >= 0) {
    if(all == NULL) {
      all = __strdup(line);
      all_len += line_len;
      continue;
    }

    all_len += line_len;

    if((all = realloc(all, all_len + 1)) == NULL) {
      fprintf(stderr, "  Error: 'read.h, (f)read_all' | invalid call to realloc()\n");
      exit(EXIT_FAILURE);
    }

    all = __strcat(all, __strdup(line));
    free(line);
    line = NULL;
  }

  return all;
}


char *fread_string(FILE *fp)
{
  char string[1000];

  if((fscanf(fp, "%999s", string)) <= 0) {
    return NULL;
  }

  return __strdup(string);
}


int fread_int(FILE *fp)
{
  fpos_t pos;
  fgetpos(fp, &pos);

  char *num_s = fread_string(fp);

  if(num_s == NULL) {
    fprintf(stderr, "  Error: 'read.h, (f)read_int' | expected an int\n");
    exit(EXIT_FAILURE);
  }

  if(!isdigit(num_s[0]) && num_s[0] != EOF && num_s[0] != '-' && num_s[0] != '.') {
    fsetpos(fp, &pos);
    fprintf(stderr, "  Error: 'read.h, (f)read_int' | expected an int\n");
    exit(EXIT_FAILURE);
  }

  int d = (int)strtol(num_s, NULL, 10);
  free(num_s), num_s = NULL;
  return d;
}


float fread_float(FILE *fp)
{
  fpos_t pos;
  fgetpos(fp, &pos);

  char *num_s = fread_string(fp);

  if(num_s == NULL) {
    fprintf(stderr, "  Error: 'read.h, (f)read_int' | expected a float\n");
    exit(EXIT_FAILURE);
  }

  if(!isdigit(num_s[0]) && num_s[0] != EOF && num_s[0] != '-' && num_s[0] != '.') {
    fsetpos(fp, &pos);
    fprintf(stderr, "  Error: 'read.h, (f)read_float' | expected a float\n");
    exit(EXIT_FAILURE);
  }

  float f = strtof(num_s, NULL);
  free(num_s), num_s = NULL;
  return f;
}

double fread_double(FILE *fp)
{
  fpos_t pos;
  fgetpos(fp, &pos);

  char *num_s = fread_string(fp);

  if(!isdigit(num_s[0]) && num_s[0] != EOF && num_s[0] != '-' && num_s[0] != '.') {
    fsetpos(fp, &pos);
    fprintf(stderr, "  Error: 'read.h, (f)read_double' | expected a double\n");
    exit(EXIT_FAILURE);
  }

  double d = (double)strtod(num_s, NULL);
  free(num_s), num_s = NULL;
  return d;
}

long fread_long(FILE *fp)
{
  fpos_t pos;
  fgetpos(fp, &pos);

  char *num_s = fread_string(fp);

  if(!isdigit(num_s[0]) && num_s[0] != EOF && num_s[0] != '-' && num_s[0] != '.') {
    fsetpos(fp, &pos);
    fprintf(stderr, "  Error: 'read.h, (f)read_long' | expected a long\n");
    exit(EXIT_FAILURE);
  }

  long l = strtol(num_s, NULL, 10);
  free(num_s), num_s = NULL;
  return l;
}

short fread_short(FILE *fp)
{
  fpos_t pos;
  fgetpos(fp, &pos);

  char *num_s = fread_string(fp);

  if(!isdigit(num_s[0]) && num_s[0] != EOF && num_s[0] != '-' && num_s[0] != '.') {
    fsetpos(fp, &pos);
    fprintf(stderr, "  Error: 'read.h, (f)read_short' | expected a short\n");
    exit(EXIT_FAILURE);
  }

  short s = (short)strtol(num_s, NULL, 10);
  free(num_s), num_s = NULL;
  return s;
}

char **fread_all_strings(FILE *fp)
{
  char **all = (char **)malloc(sizeof(char *));

  if(all == NULL) {
    fprintf(stderr, "  Error: 'read.h, (f)read_all_strings' | invalid call to malloc()\n");
    exit(EXIT_FAILURE);
  }

  size_t n = 0;

  while((all[(int)n] = fread_string(fp)) != NULL) {
    n++;
    all = (char **) realloc((void *)all, (n + 1) * sizeof(char *));

    if(all == NULL) {
      fprintf(stderr, "  Error: 'read.h, (f)read_all_strings' | invalid call to realloc()\n");
      exit(EXIT_FAILURE);
    }
  }

  // no strings were read, so free the memory
  if(n == 0) {
    free(all), all = NULL;
  }

  return all;
}

char **fread_all_lines(FILE *fp)
{
  char **all = (char **)malloc(sizeof(char *));

  if(all == NULL) {
    fprintf(stderr, "  Error: 'read.h, (f)read_all_lines' | invalid call to malloc()\n");
    exit(EXIT_FAILURE);
  }

  size_t n = 0;

  while((all[(int)n] = fread_line(fp)) != NULL) {
    n++;
    all = (char **) realloc((void *)all, (n + 1) * sizeof(char *));

    if(all == NULL) {
      fprintf(stderr, "  Error: 'read.h, (f)read_all_lines' | invalid call to realloc()\n");
      exit(EXIT_FAILURE);
    }
  }

  // no strings were read, so free the memory
  if(n == 0) {
    free(all), all = NULL;
  }

  return all;
}

int is_empty()
{
  return fis_empty(stdin);
}

int has_next()
{
  return (is_empty() == 1) ? 0 : 1;
}

char *read_line()
{
  return fread_line(stdin);
}

char *read_all()
{
  return fread_all(stdin);
}

char *read_string()
{
  return fread_string(stdin);
}

int read_int()
{
  return fread_int(stdin);
}


float read_float()
{
  return fread_float(stdin);
}

double read_double()
{
  return fread_double(stdin);
}

long read_long()
{
  return fread_long(stdin);
}

short read_short()
{
  return fread_short(stdin);
}

char **read_all_strings()
{
  return fread_all_strings(stdin);
}

char **read_all_lines()
{
  return fread_all_lines(stdin);
}

// HELPER FUNCTIONS

// returns the length of a string
static size_t __strlen(const char *s)
{
  char *p = (char *)s;

  while(*p) {
    p++;
  }

  return (unsigned char *) p - (unsigned char *) s;
}

// appends the string "src" onto "dest"
static char *__strcat(char *dest, const char *src)
{
  char *p = dest;

  while(*p) {
    p++;
  }

  while((*p++ = *src++))
    ;

  return dest;
}

// duplicates a string
static char *__strdup(char *s)
{
  size_t n = __strlen(s);
  char *ret = (char *)malloc(sizeof(char) * (n + 1));

  if(ret == NULL) {
    fprintf(stderr, "  Error: 'read.h, __strdup' | invalid call to malloc()\n");
    exit(EXIT_FAILURE);
  }

  char *p1 = s, *p2 = ret;

  while((*p2++ = *p1++))
    ;

  return ret;
}

#endif // READ_H
