#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "../../read.h"
#include "../../hashtable.h"

void test__hash_string()
{
  hashtable *ht = NULL;
  hashtable_init(&ht, 2039, string_hash, _strcmp, free_string, free_string);

  char *s, *p;
  while((s = read_string()) != NULL) {
    p = read_string();
    printf("inserting %s => %s\n", s, p);
    hashtable_insert(ht, s, p);
  }

  s = "key";
  p = (char *)hashtable_find(ht, s);

  assert(p != NULL);

  printf("lookup on %s resulted in %s\n", s, p);

  assert(!strcmp(p, "value"));

  hashtable_print(ht, print_string, print_string);
  hashtable_free(ht);
}

void test__hash_int()
{
  hashtable *ht = NULL;
  hashtable_init(&ht, 2039, int_hash, _intcmp, free_int, free_string);
  printf("table len: %d\n", (int)ht->len);

  int s;
  char *p;
  int N = read_int();

  for(int i = 0; i < N; i++) {
    s = read_int();
    p = read_string();
    hashtable_insert(ht, (void *)((long)s), p);
  }

  s = 10;
  p = (char *)hashtable_find(ht, (void *)((long)s));

  assert(p != NULL);

  printf("lookup on %d resulted in %s\n", s, p);

  assert(!strcmp(p, "value"));

  hashtable_print(ht, print_int, print_string);
  hashtable_free(ht);
}


void test_func(const char *restrict func)
{
  if(!strcmp(func, "string")) {
    test__hash_string();
  } else if(!strcmp(func, "int")) {
    test__hash_int();
  } else {
    printf("Invalid function: %s\n", func);
  }
}

void usage(const char *restrict prog)
{
  printf("  Usage: %s [function-list]\n", prog);
  exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{

  if(argc < 2) {
    usage(*argv);
  }

  for(int i = 1; i < argc; i++) {
    test_func(argv[i]);
  }

  return 0;
}
