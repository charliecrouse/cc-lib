#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "../../graph.h"

void test__graph(char *restrict filename)
{
  graph *g = NULL;
  graph_init_from_file(&g, filename);

  assert(g != NULL);

  graph_print(g);
  graph_free(g);

  printf("\n\ntest passed\n\n");
}

void usage(const char *restrict prog)
{
  fprintf(stderr, "  usage: %s [graph-data-file]\n", prog);
  exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
  if(argc < 2)
    usage(*argv);

  test__graph(argv[1]);

  return 0;
}