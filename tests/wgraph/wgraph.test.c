#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "../../wgraph.h"

void test__wgraph(char *restrict filename)
{
  wgraph *wg = NULL;
  wgraph_init_from_file(&wg, filename);

  assert(wg != NULL);

  wgraph_print(wg);
  wgraph_free(wg);

  printf("\n\ntest passed\n\n");
}

void usage(const char *restrict prog)
{
  fprintf(stderr, "  usage: %s [wgraph-data-file]\n", prog);
  exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
  if(argc < 2)
    usage(*argv);

  test__wgraph(argv[1]);

  return 0;
}