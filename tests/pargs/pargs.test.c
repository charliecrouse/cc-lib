#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "../../read.h"
#include "../../pargs.h"
#include "../../hashtable.h"

void test__pargs(char **argv)
{
  hashtable *h = pargs_parse(argv);

  while(1) {
    printf("  Enter the option to check: ");
    char *option = read_string();
    char *res = pargv_get(h, option);

    printf("    %s is set to %s\n\n", option, res);
  }

  pargv_free(h);
}

void usage(const char *restrict prog)
{
  printf("  Usage: %s arg1 arg2 ... argN", prog);
  exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{

  if(argc < 2) {
    usage(*argv);
  }

  test__pargs(argv);

  return 0;
}
