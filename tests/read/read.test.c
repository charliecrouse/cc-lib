#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include "../../read.h"


void test__is_empty()
{
  printf("TEST 1\nPRESS <ctrl-d> to continue.\n");

  assert(is_empty());

  ungetc(' ', stdin);
  ungetc('\n', stdin);
  ungetc('\t', stdin);

  assert(is_empty());

  ungetc('a', stdin);

  assert(!is_empty());

  printf("\ntest passed\n");
}

void test__has_next()
{
  printf("TEST 2\nPRESS <ctrl-d> to continue.\n");

  assert(!has_next());

  ungetc(' ', stdin);
  ungetc('\n', stdin);
  ungetc('\t', stdin);

  assert(!has_next());

  ungetc('a', stdin);

  assert(has_next());

  printf("\ntest passed\n");
}

void test__read_line()
{
  printf("TEST 3\ntype '%s<enter>%s<enter>' to continue.\n",
         "This is a line",
         "  This is another line  ");

  assert(!strcasecmp(read_line(), "This is a line"));

  assert(!strcasecmp(read_line(), "  This is another line  "));

  printf("\ntest passed\n");
}

void test__read_all()
{
  printf("TEST 4\ntype 'There<enter>is<enter>a lot<enter>to read<enter><enter><ctrl-d>' to continue.\n");

  assert(!strcasecmp(read_all(), "There\nis\na lot\nto read\n\n"));

  printf("\ntest passed\n");
}

void test__read_string()
{
  printf("TEST 5\ntype '%s<space>%s<enter>%s<enter><tab>%s<enter>' to continue.\n",
         "Hello",
         "World",
         "from",
         "stdin");

  assert(!strcasecmp(read_string(), "Hello"));

  assert(!strcasecmp(read_string(), "World"));

  assert(!strcasecmp(read_string(), "from"));

  assert(!strcasecmp(read_string(), "stdin"));

  printf("\ntest passed\n");
}

void test__read_int()
{
  printf("TEST 5\ntype '%d %d %d %d<enter>' to continue.\n",
         5,
         -5,
         INT_MIN,
         INT_MAX);

  assert(read_int() == 5);

  assert(read_int() == -5);

  assert(read_int() == INT_MIN);

  assert(read_int() == INT_MAX);

  printf("\ntest passed\n");
}

void test__read_float()
{
  printf("TEST 6\ntype '%f %f %f %f<enter>' to continue.\n",
         5.5,
         -5.5,
         .123,
         -.123);

  assert(read_float() == (float) 5.5);

  assert(read_float() == (float) - 5.5);

  assert(read_float() == (float) .123);

  assert(read_float() == (float) - .123);

  printf("\ntest passed\n");
}

void test__read_double()
{
  printf("TEST 7\ntype '%lf %lf %lf %lf<enter>' to continue.\n",
         10.123456,
         -10.123456,
         .123456,
         -.123456);

  assert(read_double() == (double)10.123456);

  assert(read_double() == (double) - 10.123456);

  assert(read_double() == (double).123456);

  assert(read_double() == (double) - .123456);

  printf("\ntest passed\n");
}

void test__read_long()
{
  printf("TEST 8\ntype '%ld %ld %ld %ld<enter>' to continue.\n",
         100L,
         -100L,
         LONG_MIN,
         LONG_MAX);

  assert(read_long() == (long)100);

  assert(read_long() == (long) - 100);

  assert(read_long() == (long)LONG_MIN);

  assert(read_long() == (long)LONG_MAX);

  printf("\ntest passed\n");
}

void test__read_short()
{
  printf("TEST 9\ntype '%hd %hd %hd %hd<enter> to continue.\n",
         (short)100,
         (short) - 100,
         (short)SHRT_MIN,
         (short)SHRT_MAX);

  assert(read_short() == (short)100);

  assert(read_short() == (short) - 100);

  assert(read_short() == (short)SHRT_MIN);

  assert(read_short() == (short)SHRT_MAX);

  printf("\ntest passed\n");
}

void test__read_all_strings()
{
  printf("TEST 10\ntype '%s<enter><ctrl-d>' to continue.\n",
         "there are a lot of strings");

  char **strings = read_all_strings();

  assert(strings != NULL);

  assert(!strcasecmp(strings[0], "there"));

  assert(!strcasecmp(strings[1], "are"));

  assert(!strcasecmp(strings[2], "a"));

  assert(!strcasecmp(strings[3], "lot"));

  assert(!strcasecmp(strings[4], "of"));

  assert(!strcasecmp(strings[5], "strings"));

  printf("\ntest passed\n");
}

void test__read_all_lines()
{
  printf("TEST 11\ntype '%s<enter>%s<enter>%s<enter><ctrl-d>' to continue.\n",
         "this is the first line",
         "    this line is indented",
         "and this is normal");

  char **lines = read_all_lines();

  assert(lines != NULL);

  assert(!strcasecmp(lines[0], "this is the first line"));

  assert(!strcasecmp(lines[1], "    this line is indented"));

  assert(!strcasecmp(lines[2], "and this is normal"));

  printf("\ntest passed\n");
}

void test_func(const char *restrict func)
{
  if(!strcmp(func, "is_empty")) {
    test__is_empty();
  } else if(!strcmp(func, "has_next")) {
    test__has_next();
  } else if(!strcmp(func, "read_line")) {
    test__read_line();
  } else if(!strcmp(func, "read_all")) {
    test__read_all();
  } else if(!strcmp(func, "read_string")) {
    test__read_string();
  } else if(!strcmp(func, "read_int")) {
    test__read_int();
  } else if(!strcmp(func, "read_float")) {
    test__read_float();
  } else if(!strcmp(func, "read_double")) {
    test__read_double();
  } else if(!strcmp(func, "read_long")) {
    test__read_long();
  } else if(!strcmp(func, "read_short")) {
    test__read_short();
  } else if(!strcmp(func, "read_all_strings")) {
    test__read_all_strings();
  } else if(!strcmp(func, "read_all_lines")) {
    test__read_all_lines();
  } else {
    printf("Invalid function: %s\n", func);
  }
}

void usage(const char *restrict prog)
{
  printf("  Usage: %s [function-list]\n", prog);
  exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{

  if(argc < 2) {
    usage(*argv);
  }

  for(int i = 1; i < argc; i++) {
    test_func(argv[i]);
  }

  return 0;
}

