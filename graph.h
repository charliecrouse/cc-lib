/**
 * Unweighted, Undirected Graph
 * @author Charlie Crouse
 * @version 1.0.0
 * @since 2017-07-30
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is a part of a collection of C programs that construct a library  *
 *  meant for personal use.                                                    *
 *                                                                             *
 *  Copyright (C) 2017 Charlie T. Crouse                                       *
 *                                                                             *
 *  This program is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation, either version 3 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef GRAPH_H
#define GRAPH_H

#include <stdio.h>
#include <stdlib.h>

typedef struct node {
  int source;
  struct node *next;
} node;

typedef struct graph {
  int V;
  int E;
  node **adj_list;
} graph;

/**
 * allocates a buffer for a node
 * @return the node buffer
 */
node *node_alloc();

/**
 * initializes and allocates memory for a node
 * @param node **P A pointer to the node to initialize
 * @param int source The source vertex for the node
 */
void node_init(node **p, int source);

/**
 * allocates a buffer for a graph
 * @return the graph buffer
 */
graph *graph_alloc();

/**
 * initializes and allocates memory for a node
 * @param graph **g A pointer to the graph to initialize
 * @param int V The number of vertexes the graph should contain
 */
void graph_init(graph **g, int V);

/**
 * initializes a graph from data contained in a given file
 * @note the file should contain the number of vertexes, then the number of
 * edges, then list the edges as two integers followed by a new line
 * @param g        A pointer to the graph to initialize
 * @param filename The name of the file that contains the data
 */
void graph_init_from_file(graph **g, char *filename);

/**
 * frees all allocated resources for a graph
 * @param g A pointer to the graph to free
 */
void graph_free(graph *g);

/**
 * adds a connection from 'v' to 'w'.
 * @param g A pointer to a graph to add the connection to
 * @param v The first vertex
 * @param w The second vertex
 */
void graph_add_edge(graph *g, int v, int w);

/**
 * determines if two vertexes are connected
 * @param  g A pointer to a graph that will be checked for the connection
 * @param  v The first vertex
 * @param  w The second vertex
 * @return   (1) if the two vertexes are adjacent and (0) otherwise
 */
int graph_connected(graph *g, int v, int w);

/**
 * prints a formatted graph to stdout
 * @param g A pointer to a graph to print
 */
void graph_print(graph *g);


// ---------------------------- Implementations ----------------------------- //

static void validate_vertex(graph *g, int v);

node *node_alloc()
{
  node *p = (node *)malloc(sizeof(node));

  if(p == NULL) {
    fprintf(stderr, "  Error: 'graph.h, node_alloc' | invalid call to malloc()\n");
    exit(EXIT_FAILURE);
  }

  return p;
}

void node_init(node **p, int source)
{
  if(*p == NULL) {
    *p = node_alloc();
  }

  (*p)->source = source;
  (*p)->next   = NULL;
}

graph *graph_alloc()
{
  graph *g = (graph *)malloc(sizeof(graph));

  if(g == NULL) {
    fprintf(stderr, "  Error: 'graph.h, graph_alloc' | invalid call to malloc()\n");
    exit(EXIT_FAILURE);
  }

  return g;
}

void graph_init(graph **g, int V)
{
  if(*g == NULL) {
    *g = graph_alloc();
  }

  (*g)->V = V;
  (*g)->E = 0;

  (*g)->adj_list = (node **)malloc(sizeof(node) * V);

  if((*g)->adj_list == NULL) {
    fprintf(stderr, "  Error: 'graph.h, graph_init' | invalid call to malloc()\n");
    exit(EXIT_FAILURE);
  }

  for(int i = 0; i < V; i++) {
    (*g)->adj_list[i] = NULL;
  }
}

void graph_init_from_file(graph **g, char *filename)
{
  FILE *fp = fopen(filename, "r");

  if(fp == NULL) {
    fprintf(stderr, "  Error: 'graph.h, graph_init_from_file' | invalid file \"%s\"\n", filename);
    exit(EXIT_FAILURE);
  }

  if(*g != NULL) {
    graph_free(*g), *g = NULL;
  }

  int V, N;
  fscanf(fp, "%d\n%d", &V, &N);

  graph_init(g, V);

  int v, w;

  while(fscanf(fp, "%d %d", &v, &w) > 0) {
    validate_vertex(*g, v);
    validate_vertex(*g, w);
    graph_add_edge(*g, v, w);
  }
}

void graph_free(graph *g)
{
  if(g == NULL) {
    fprintf(stderr, "  Error: 'graph.h, graph_free' | graph is NULL\n");
    exit(EXIT_FAILURE);
  }

  for(int i = 0; i < g->V; i++) {
    node *p, *s;
    p = g->adj_list[i];

    while((s = p) != NULL) {
      p = p->next;
      free(s);
      s = NULL;
    }

    p              = NULL;
    g->adj_list[i] = NULL;
  }

  free(g->adj_list);
  g->adj_list = NULL;
  free(g);
  g = NULL;
}

void graph_add_edge(graph *g, int v, int w)
{
  if(g == NULL) {
    fprintf(stderr, "  Error: 'graph.h, graph_add_edge' | graph is null\n");
    exit(EXIT_FAILURE);
  }

  validate_vertex(g, v);
  validate_vertex(g, w);

  node *p = NULL;
  node_init(&p, w);
  p->next        = g->adj_list[v];
  g->adj_list[v] = p;

  node *s = NULL;
  node_init(&s, v);
  s->next        = g->adj_list[w];
  g->adj_list[w] = s;
}

int graph_connected(graph *g, int v, int w)
{
  if(g == NULL) {
    fprintf(stderr, "  Error: 'graph.h, graph_connected' | graph is null\n");
    exit(EXIT_FAILURE);
  }

  node *p;

  for(p = g->adj_list[v]; p != NULL; p = p->next) {
    if(p->source == w) {
      return 1;
    }
  }

  for(p = g->adj_list[w]; p != NULL; p = p->next) {
    if(p->source == v) {
      return 1;
    }
  }

  return 0;
}

void graph_print(graph *g)
{
  if(g == NULL) {
    fprintf(stderr, "  Error: 'graph.h, graph_print' | graph is null\n");
    exit(EXIT_FAILURE);
  }

  for(int i = 0; i < g->V; i++) {
    printf("%d: ", i);

    for(node *p = g->adj_list[i]; p != NULL; p = p->next) {
      printf("%d%s", p->source, (p->next == NULL) ? "" : ", ");
    }

    printf("\n");
  }
}

static void validate_vertex(graph *g, int v)
{
  if(v >= g->V) {
    fprintf(stderr, "  Error: 'graph.h' | vertex %d is not less than %d\n", v, g->V);
    exit(EXIT_FAILURE);
  }
}

#endif // GRAPH_H
